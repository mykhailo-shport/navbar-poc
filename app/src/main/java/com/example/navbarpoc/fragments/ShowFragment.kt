package com.example.navbarpoc.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.navbarpoc.R
import kotlinx.android.synthetic.main.fragment_show.*

class ShowFragment : Fragment(R.layout.fragment_show) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        episode.requestFocus()
    }
}