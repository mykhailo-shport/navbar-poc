package com.example.navbarpoc

import androidx.fragment.app.Fragment
import com.example.navbarpoc.fragments.*

object Router {

    fun getNavigationFragment(permalink: String): Fragment {
        return when (permalink) {
            "home" -> HomeFragment()
            "live" -> LiveFragment()
            "movies" -> MoviesFragment()
            else -> ErrorFragment()
        }
    }

    fun getFragment(permalink: String): Fragment {
        return when (permalink) {
            "show" -> ShowFragment()
            else -> ErrorFragment()
        }
    }

}