package com.example.navbarpoc.components.navbar

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.navbarpoc.R
import kotlinx.android.synthetic.main.nav_bar.view.*

class NavBar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {


    init {
        LayoutInflater.from(context).inflate(R.layout.nav_bar, this)
    }

    fun setNavBar(list: List<NavBarItem>) {
        rvTopNavMenu.adapter = NavBarAdapter(list)
    }

    fun setOnFocusChangeListener(listener: (NavBarItem) -> Unit) {
        (rvTopNavMenu.adapter as NavBarAdapter).setOnFocusChangeListener(listener)
    }
}