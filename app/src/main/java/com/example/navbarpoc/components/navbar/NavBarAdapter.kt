package com.example.navbarpoc.components.navbar

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.navbarpoc.R

class NavBarAdapter(private val navBarItems: List<NavBarItem>) : RecyclerView.Adapter<NavBarAdapter.Companion.ViewHolder>() {

    companion object {
        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val title: TextView = itemView.findViewById(R.id.title)
        }
    }

    private var onFocusChangeListener: ((NavBarItem) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_nav_bar, parent, false))
    }

    fun setOnFocusChangeListener(listener: (NavBarItem) -> Unit) {
        onFocusChangeListener = listener
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val navBarItem: NavBarItem = navBarItems[position]
        holder.title.text = navBarItem.title
        holder.title.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                onFocusChangeListener?.invoke(navBarItem)
            }
        }
    }

    override fun getItemCount(): Int {
        return navBarItems.size
    }
}