package com.example.navbarpoc.fragments

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.navbarpoc.R
import com.example.navbarpoc.NavigateListener
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment: Fragment(R.layout.fragment_home) {

    private var navigateListener: NavigateListener? = null

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        if (activity is NavigateListener) {
            navigateListener = activity
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        show.setOnClickListener {
            navigateListener?.navigateTo("show")
        }
    }

}