package com.example.navbarpoc

interface NavigateListener { // probably would be better to use live data
    fun navigateTo(permalink: kotlin.String)
}