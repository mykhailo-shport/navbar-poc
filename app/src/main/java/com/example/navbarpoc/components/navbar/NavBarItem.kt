package com.example.navbarpoc.components.navbar

data class NavBarItem(val title: String, val permalink: String) {
}