package com.example.navbarpoc.micro_app

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE
import com.example.navbarpoc.R
import com.example.navbarpoc.Router
import com.example.navbarpoc.NavigateListener
import com.example.navbarpoc.components.navbar.NavBarItem
import kotlinx.android.synthetic.main.activity_main.*

class NavBarActivity : FragmentActivity(), NavigateListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navBar.setNavBar(
            listOf(
                NavBarItem("Home", "home"),
                NavBarItem("Live", "live"),
                NavBarItem("Movies","movies")
            )
        )
        navBar.setOnFocusChangeListener {
            if (supportFragmentManager.backStackEntryCount > 0) {
                replaceFragmentAndClearStack(Router.getNavigationFragment(it.permalink))
            } else {
                replaceFragment(Router.getNavigationFragment(it.permalink))
            }
        }
        navBar.requestFocus()

        supportFragmentManager.addOnBackStackChangedListener {
            Log.d(
                NavBarActivity::class.java.simpleName,
                "backstack entry count - ${supportFragmentManager.backStackEntryCount}"
            )
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack(null, POP_BACK_STACK_INCLUSIVE)
        } else {
            super.onBackPressed()
        }
    }

    private fun addFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.fragmentContainer, fragment)
            .addToBackStack(null) // todo we would need tag name for each fragment
            .commit()
    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, fragment)
            .commit()
    }

    private fun replaceFragmentAndClearStack(fragment: Fragment) {
        supportFragmentManager.popBackStackImmediate(0, POP_BACK_STACK_INCLUSIVE)

        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, fragment)
            .commit()
    }

    override fun navigateTo(permalink: String) {
        addFragment(Router.getFragment(permalink))
    }

}
